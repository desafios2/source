# Linguagens

- Java
- C++
- Python

# IDEs

- Java:   Apache NetBeans 12.1
- C++:    Visual Studio Code 1.50.1
- Python: Visual Studio Code 1.50.1

# Compiladores

- Java:     openjdk version "11.0.8" 2020-07-14
- C++:      gcc (Ubuntu 9.3.0-17ubuntu1~20.04) 9.3.0
- Python:    Python 3.8.5

# Sistema Operacional

- Ubuntu 20.04.1 LTS