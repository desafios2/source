def checkBoard(table,board):
    #identificador de cada peca
    peao = 0
    cavalo = 0
    bispo = 0
    torre = 0
    rei = 0
    rainha = 0

    #contadores para cada peca
    p_count = 0
    c_count = 0
    b_count = 0
    t_count = 0
    rei_count = 0
    rainha_count = 0

    #percorrendo a entradas das pecas e armazenando os valores de cada peca em seus respectivos indicadores
    for key,value in table.items():
        if key == "peao":
            peao = value
        elif key == "cavalo":
            cavalo = value
        elif key == "bispo":
            bispo = value
        elif key == "torre":
            torre = value
        elif key == "rei":
            rei = value
        elif key == "rainha":
            rainha = value

    #transformando o restado da porta logica OU exclusivo(XOR) -> 1: TRUE e 0:FALSE ; caso o valor seja 1,incrementa o contador
    #(mais informacoes no metodo comparator)
    for i in range(8):
        for j in range(8):
            p_count+=int(xorCompare(board[i][j],peao))
            c_count+=int(xorCompare(board[i][j],cavalo))
            b_count+=int(xorCompare(board[i][j],bispo))
            t_count+=int(xorCompare(board[i][j],torre))
            rei_count+=int(xorCompare(board[i][j],rei))
            rainha_count+=int(xorCompare(board[i][j],rainha))

    #print
    print(f'Peao: {p_count} peca(s)')
    print(f'Bispo: {b_count} peca(s)')
    print(f'Cavalo: {c_count} peca(s)')
    print(f'Torre: {t_count} peca(s)')
    print(f'Rainha: {rainha_count} peca(s)')
    print(f'Rei: {rei_count} peca(s)')

    #armazendo as respostas
    return {"peao":p_count,
            "bispo":b_count,
            "cavalo":c_count,
            "torre":t_count,
            "rainha":rainha_count,
            "rei":rei_count}

"""
    utilizando a porta logica: ou exclusivo(XOR) para comparar dois inteiros,
    caso eles sejam iguais a comparacao retornaria 0 em todos os bits, logo o compilador transforma esse 0 em FALSE
    caso os numeros sejam diferentes o resultado seria 1 ou TRUE, eu utilizei o `not` para inverter o resultado transformando
    1 : caso iguais e 0: caso diferentes, logo a porta esta sendo usada como um comparador e um contador ao mesmo tempo.
"""
def xorCompare(i,j):
    return not(i ^ j)