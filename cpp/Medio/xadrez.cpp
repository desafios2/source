#include <iostream>
#include <vector>
#include <bitset>
#include <string>

using namespace std;

class boardChecker{
    public:
        vector<pair<string,int>> check(int board[8][8],vector<pair<string,int>> pcs){
            vector<pair<string,int>> res;

            //contadores para cada peca
            int peao = 0;
            int bispo = 0;
            int cavalo = 0;
            int torre = 0;
            int rainha = 0;
            int rei = 0;
            
            // identificador de cada peca 
            int peao_const = 0;
            int bispo_const = 0;
            int cavalo_const = 0;
            int torre_const = 0;
            int rainha_const = 0;
            int rei_const = 0;

            //percorrendo a entradas das pecas e armazenando os valores de cada peca em seus respectivos
            //identificadores
            for(pair<string,int> p : pcs){
                if(p.first == "peao"){
                    peao_const = p.second;
                }else if(p.first == "bispo"){
                    bispo_const = p.second;
                }else if(p.first == "cavalo"){
                    cavalo_const = p.second;
                }else if(p.first == "torre"){
                    torre_const = p.second;
                }else if(p.first == "rainha"){
                    rainha_const = p.second;
                }else if(p.first == "rei"){
                    rei_const = p.second;
                }

            }

            //transformando o restado da porta logica OU exclusivo(XOR) -> 1: TRUE e 0:FALSE ; caso o valor seja 1,incrementa o contador
            //(mais informacoes no metodo comparator)
            for (int i = 0; i < 8; i++){
                for (int j = 0; j < 8; j++){
                        peao+= (int)comparator(peao_const,board[i][j]);
                        bispo+= (int)comparator(bispo_const,board[i][j]);
                        cavalo+= (int)comparator(cavalo_const,board[i][j]);
                        torre+= (int)comparator(torre_const,board[i][j]);
                        rainha+= (int)comparator(rainha_const,board[i][j]);
                        rei+= (int)comparator(rei_const,board[i][j]);
                };
            }
            //print o acumulador de cada peca
            cout << "Peao: " << peao << " peca(s)" <<endl;
            cout << "Bispo: " << bispo << " peca(s)" <<endl;
            cout << "Cavalo: " << cavalo << " peca(s)" <<endl;
            cout << "Torre: " << torre << " peca(s)" <<endl;
            cout << "Bispo: " << bispo << " peca(s)" <<endl;
            cout << "Rainha: " << rainha << " peca(s)"<<endl;
            cout << "Rei: " << rei << " peca(s)"<<endl;

            //armazendo as respostas
            res.push_back(make_pair("peao",peao));
            res.push_back(make_pair("bispo",bispo));
            res.push_back(make_pair("cavalo",cavalo));
            res.push_back(make_pair("torre",torre));
            res.push_back(make_pair("bispo",bispo));
            res.push_back(make_pair("rainha",rainha));
            res.push_back(make_pair("rei",rei));

            return res;
        };

private:
        /*
            utilizando a porta logica: ou exclusivo(XOR) para comparar dois inteiros,
            caso eles sejam iguais a comparacao retornaria 0 em todos os bits, logo o compilador transforma esse 0 em FALSE
            caso os numeros sejam diferentes o resultado seria 1 ou TRUE, eu utilizei o `!` para inverter o resultado transformando
            1 : caso iguais e 0: caso diferentes, logo a porta esta sendo usada como um comparador e um contador ao mesmo tempo.
        */
        bool comparator(int i ,int j){
            return !(i ^ j);
        }
};