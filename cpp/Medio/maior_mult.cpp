#include <iostream>
#include <vector>

using namespace std;

class maior_mult {
public:
    vector<int> getSubArray(int arr[],int n) {
        //armazena os numeros positivos
        vector<int> vars;
        //armazena os numeros negativos
        vector<int> vars_neg;

        //percorre o array separando os numeros em negativos e positivos(0 eh sempre ignorado)
        for (int i = 0; i < n; i++) {
            if(arr[i] > 0){
                vars.emplace_back(arr[i]);
            }else if(arr[i] < 0){
                vars_neg.emplace_back(arr[i]);
            }else{
                continue;
            }
        }

        //se a quantidade de numeros negativos em um array for impar: retirar o mais proximo de 0
        //se a quantidade for par a multiplicacao dos sinais sempre vai resultar em um numero positivo(pular o if)
        if(vars_neg.size() % 2 != 0){
            int maior = INT32_MIN;
            int index = 0;
            for(int i = 0 ; i < vars_neg.size() ; i++){
                if(vars_neg[i] > maior){
                    maior = vars_neg[i];
                    index = i;
                }
            }
            vars_neg.erase(vars_neg.cbegin()+index);
        }

        //inserindo os numeros negativos na resposta(array de numeros positivos)
        for(int i = 0 ; i < vars_neg.size() ; i++){
            vars.emplace_back(vars_neg[i]);
        }

        //print
        cout << "{ ";
        for(int i : vars){
            cout << i << " ";
        }
        cout << "}" << endl;

        return vars;
	};
};