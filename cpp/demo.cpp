#include<iostream>
#include<vector>

#include "Facil/cotacao.cpp"
#include "Facil/maior_num.cpp"

#include "Medio/maior_mult.cpp"
#include "Medio/xadrez.cpp"

#include "Dificil/sequencia.cpp"
#include "Dificil/const.cpp"

using namespace std;

int main(){

	//FACIL

	//desafio do array simples
	cout << "COTACOES" << endl << endl;
	cotacao co;

	cout << "adicionando 3 titulos:" << endl;
	co.adicionar("titulo 1");
	co.adicionar("titulo 2");
	co.adicionar("titulo 3");

	co.print_cotacoes();
	cout << "~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
	cout << "removendo `titulo 1`: " << endl;
	co.remover("titulo 1");
	co.print_cotacoes();
	cout << "~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
	cout << "removendo o titulo na posicao 1: " << endl;
	co.remover(1);
	co.print_cotacoes();
	cout << "~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
	cout << "Alterando o ultimo titulo restante: " << endl;
	co.alterar(0,"Unico Titulo");
	co.print_cotacoes();

	cout << "-------------------------------" << endl;

	//encontre o maior numero
	comparator cp;
	cout << "ENCONTRE O MAIOR NUMERO" << endl << endl;
	cout << "entre 5 e 10: " <<cp.compare(5,10) << endl;
	cout << "entre 15 e 8: "<<cp.compare(15,8) << endl;
	cout << "--------------------------------"<<endl;

	//MEDIO
	//encontre o produto maximo
	maior_mult m;
	cout << "MAIOR MULTIPLICACAO DE UM ARRAY" << endl << endl;
	int a[] = {3,5,-9,-8,0,-1,2};
	cout << "arr = {3,5,-9,-8,0,-1,2}:" << endl ;
	m.getSubArray(a,7);
	int b[] = {-6,4,-5,-2,5,0,8} ;
	cout << endl << "arr = {-6,4,-5,-2,5,0,8}:" << endl ;
	m.getSubArray(b,7);
	cout <<"-----------------------------------" << endl;
	
	//desafio de xadrez
	int board[8][8] = {	{4,3,2,5,6,2,3,4},
						{1,1,1,1,1,1,1,1},
						{0,0,0,0,0,0,0,0},
						{0,0,0,0,0,0,0,0},
						{0,0,0,0,0,0,0,0},
						{0,0,0,0,0,0,0,0},
						{1,1,1,1,1,1,1,1},
						{4,3,2,5,6,2,3,4}};
	vector<pair<string,int>> pcs;
	pcs.emplace_back(make_pair<string,int>("vazio",0));
	pcs.emplace_back(make_pair<string,int>("peao",1));
	pcs.emplace_back(make_pair<string,int>("torre",2));
	pcs.emplace_back(make_pair<string,int>("bispo",3));
	pcs.emplace_back(make_pair<string,int>("cavalo",4));
	pcs.emplace_back(make_pair<string,int>("rei",5));
	pcs.emplace_back(make_pair<string,int>("rainha",6));

	boardChecker bc;
	cout << "PECAS EM UMA MESA DE XADREZ:" << endl << endl;
	bc.check(board,pcs);
	cout << "--------------------------------------" << endl;

	//DIFICIL

	//encontre o indice do zero
	sqc sq;
	cout << "MELHOR INDICE PRA UMA SEQUENCIA DE 1:" << endl << endl;
	int c[] = {0,0,1,0,1,1,1,0,1};
	cout << "c[] = {0,0,1,0,1,1,1,0,1} -> "; 
	sq.getIndex(c,9);
	cout << "---------------------------------------" << endl;

	//Robson construcoes
	db db1;
	cout << "ROBSON CONSTRUCOES: " << endl << endl;
	db1.cadastrar_cargo(500);
	db1.cadastrar_cargo(720);
	funcionario f1(52,"matheus",1);
	funcionario f2(2,"pedro",0);
	funcionario f3(35,"adriana",1);
	funcionario f4(89,"debora",1);
	funcionario f5(63,"bruna",0);
	//funcionario invalido
	funcionario f6(55,"karine",2);

	db1.cadastrar_funcionario(f1);
	db1.cadastrar_funcionario(f2);
	db1.cadastrar_funcionario(f3);
	db1.cadastrar_funcionario(f4);
	db1.cadastrar_funcionario(f5);
	//funcionario invalido
	db1.cadastrar_funcionario(f6);

	db1.relatorio_func();

	db1.relatorio_carg(0);

	db1.relatorio_carg(1);

	return 0;
}