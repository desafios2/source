/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sources;

import java.util.ArrayList;

/**
 *
 * @author matheus
 */
public class sequencia {
    public static int check(ArrayList<Integer> entrada){
                
        //valor que armazena a maior sequencia de 1;
        int maior_sequencia = 0;
        //o indice onde se encotra o valor otimo
        int index = 0;
        
        //variavel auxiliar
        int sequencia_temp = 0;
        
        //percorrendo o vetor
        for (int i = 0; i < entrada.size(); i++) {
            //caso o valor seja zero
            if(entrada.get(i) == 0){
                
                //temporariamente transforma em 1 para verificar se o valor otimo eh encontrado
                entrada.set(i, 1);
                
                //percorre os numeros a direita de i (maiores)
                for (int j = i; j < entrada.size(); j++) {
                    //caso seja 1 o contador temporario incrementa
                    if(entrada.get(j) == 1){
                        sequencia_temp++;
                    //caso nao seja 1 o loop eh encerrado
                    }else{
                        break;
                    }
                }
                
                //percorre os numeros a esquerda de i (menores)
                for (int j = i; j > 0; j--) {
                    //caso seja 1 o contador temporario incrementa
                    if(entrada.get(j) == 1){
                        sequencia_temp++;
                    //caso nao seja 1 o loop eh encerrado
                    }else{
                        break;
                    }
                }
                
                //caso a sequencia atual seja maior que a anterior armazene a quantidade da sequencia e o indice
                if(sequencia_temp > maior_sequencia){
                    maior_sequencia = sequencia_temp;
                    index = i;
                }
                
                sequencia_temp = 0;
                
                //o valor eh transformado em zero apos o fim da verficacao
                entrada.set(i, 0);
            }
            
        }
        
        // o indice e adicionado em 1
        index++;
        
        return index;
    }
}
